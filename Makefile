.PHONY: all
all:
	@apt install fakeroot icoutils innoextract p7zip -y
	@dpkg --add-architecture i386
	@dpkg -i ./dependency/loki-compat-libs_1.5-1_i386.deb
	@export LD_LIBRARY_PATH="/usr/local/lib/loki-compat-libs"
	@cd workspace && sh play-alpha-centauri_gog-2.0.2.23.sh
	@mv ./workspace/alpha-centauri_6.0b-gog2.0.2.23_i386.deb .

.PHONY: prepare
prepare:
	@apt install fakeroot icoutils innoextract p7zip dos2unix -y

.PHONY: clean
clean:
	@rm -f alpha-centauri_6.0b-gog2.0.2.23_i386.deb > /dev/null

.PHONY: install
install:
ifndef ARCH
	@echo "ARCH is not set. Export it like: export ARCH=amd64"
	@exit 1
endif

ifeq ($(ARCH), amd64)
	@echo "requested architecture: ${ARCH}"
else ifeq ($(ARCH), i386)
	@echo "requested architecture: ${ARCH}"
else
	@echo "unknown \$ARCH: $ARCH";
	@exit 1
endif

	@echo "update apt for gathering dependencies..."
	@dpkg -i releases/loki-compat-libs_1.5-1_"${ARCH}".deb
	@-dpkg -i releases/alpha-centauri_6.0b-gog2.0.2.23_"${ARCH}".deb
	@apt update -y
	@apt --fix-broken install
	@echo ""
	@echo "ALPHA CENTAURI IS INSTALLED SUCCESSFULLY!"

.PHONY: uninstall
uninstall:
	@echo "uninstalling Alpha Centauri..."
	@dpkg --purge alpha-centauri
	@dpkg --purge loki-compat-libs
	@echo ""
	@echo "ALPHA CENTAURI IS UN-INSTALLED SUCCESSFULLY!"
