![logo](https://gitlab.com/holloway/alpha-centauri/raw/main/workspace/SidMeiers_Alpha_Centauri_PCFINAL.jpg)
# Alpha Centauri Linux DEB Packager
Alpha Centauri is a 1990s turn-based strategy games for Windows environment.
To ensure the game is capable of running on Linux, the repository compiles and
updates the legacy game to install and run painlessly on modern Linux
architectures and OS.




## Supported OS
Here are some of the supported OS

| OS            | CPU   | Status     | Last Tested        |
|:--------------|:-----:|:----------:|:------------------:|
| Debian Buster | amd64 | tested     | April 3, 2021      |




## Getting Started
Here are some quick start as an end-user.



### Installation
To install the game, simply clone the repository and execute the following:

```bash
$ git clone git@gitlab.com:holloway/alpha-centauri.git
$ export ARCH=amd64
$ make install
```

Once you see the successful installation statement, you're ready to use it.



### Launch The Game
To launch the game, simply open a terminal and do:

```
$ alpha-centauri # for classical version
$ alpha-centauri_smacx # for crossfire expansion version
```

Add a `-w` argument if you want to play in windowed mode.



### Un-Installation
To uninstall:
```bash
$ git clone git@gitlab.com:holloway/alpha-centauri.git # skip if you have it
$ export ARCH=amd64
$ make uninstall
```

Once you see the successful un-installation statement, you're free to go.



## Notable Bug
There are still some bugs.

1. When running fullscreen, sometimes, in game video will crash the game
   entirely. --Workaround by playing with windowed mode.




## Developer Notes
Some important developer notes for maintaining this repository.



### 1. You can't Make the DEB Package Without Specialized Debian OS
This game is too old and are using antique libraries way back to i386 glibc 2.X
to compile the image. Without setting up that kind of environment, chances are,
you will compile a SMAC image with broken Jackal program initialization (seen
in Debian Buster 10). Also, keep in mind that i386 CPU are obselete and it is
rare to get support or hardware for it.

I only manage to compile a copy of it from scratch using Loki's guides
(`alpha-centauri_6.0b-gog2.0.2.23_i386.deb` saved in the `releases/` directory.
The only thing I do is to identify and edit the deb package control files to
match modern CPU architectures and dependencies.

In case you go all the way back, I retain the build instructions as follow:

```
$ git clone git@gitlab.com:holloway/alpha-centauri.git
$ cd alpha-centauri
$ make prepare
$ make all
```

Good luck drilling down the bottomless hole.



### 2. Hidden dependencies
There were a number of hidden dependencies Loki team did not specified (due to
their company fate) during Debian 10 porting. Among them are:

1. `loki-compat-libs:amd64` - tested `(>= 1.5)`
2. `libsdl-image1.2:amd64` - tested `(>= 1.2)`
3. `libsmpeg0:amd64` - tested `(>= 0.4)`
4. `libpulse0:amd64` - tested `(>= 12.0)`
5. `osspd:amd64` - tested `(>= 1.3)`

Only the `amd64` packages are tested. The i386 will remain as master copy until
another broken changes in the future. (Also, I don't have any i386 CPU lying
around and neither I want to support them).



### 3. No `arm` CPU support
This game is a legacy game. To support ARM, it needs to be recompiled entirely.
Since this is a private game, there is no arm architecture support for it.
I do not have its source codes anyway.



### 4. Use `workspace/deb-editor.bash` to Update Package Control
As technologies are moving forward, it's best to update the package control file
instead of recompile. There is a work script available at your disposal. Please
feel free to update the package as time progresses.
