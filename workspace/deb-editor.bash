#!/bin/bash




# check dependencies
_list=(
	"vim"
	"dpkg-deb"
	"dpkg"
)
for i in "${_list[@]}"; do
	if [ "$(type -p "$i")" == "" ]; then
		echo "[ ERROR ] - missing dependency: $i"
		exit 1
	fi
done




# sort of variables
DEB_PACKAGE="$1"
OUTPUT_PACKAGE="$(basename "$DEB_PACKAGE" .deb).modfied.deb"
if [ ! -f "$DEB_PACKAGE" ]; then
	echo "[ ERROR ] - missing deb package: $DEB_PACKAGE"
	exit 1
fi




# create workspace to work with
WORKSPACE_DIR="$(mktemp -d /tmp/deb.XXXXXXXXXX)"
if [ "$?" -ne 0 ]; then
	exit 1
fi




# extract the deb package to workspace directory
dpkg-deb -x "$DEB_PACKAGE" "$WORKSPACE_DIR"
dpkg-deb --control "$DEB_PACKAGE" "$WORKSPACE_DIR"/DEBIAN
CONTROL_FILE="${WORKSPACE_DIR}/DEBIAN/control"
if [ ! -f "$CONTROL_FILE" ]; then
	echo "[ ERROR ] - missing control file"
	rm -rf "$WORKSPACE_DIR" &> /dev/null
	exit 1
fi




# prepare editors to modify the control file
MOD=$(stat -c "%y" "$CONTROL_FILE")
vim "$CONTROL_FILE"




# recompile
rm "$OUTPUT_PACKAGE" &> /dev/null
if [ "$MOD" == "$(stat -c "%y" $CONTROL_FILE)" ]; then
	echo "[ WARNING ] control file is not modified."
	cp "$DEB_PACKAGE" "$OUTPUT_PACKAGE"
else
	echo "[ INFO    ] building the modified package..."
	dpkg -b "$WORKSPACE_DIR" "$OUTPUT_PACKAGE"
fi




# remove workspace
rm -r "$WORKSPACE_DIR"
